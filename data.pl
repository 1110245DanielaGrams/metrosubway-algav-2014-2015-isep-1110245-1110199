%places to visit --> place name, opening hours, closing hours, closest metro line, distance in minutes, average visiting time

%localTuristico(the_palaces_of_versailles,540,1110,train_line_c,20,180). commented, no metro line there
localTuristico(the_arc_de_triomphe,600,1380,charles_de_gaulle_etoile,5,30). 
localTuristico(the_louvre_museum,540,1080,louvre_rivoli,20,360).
localTuristico(cathedral_of_notre_dame,480,1125,cite,10,180).
localTuristico(champs_elyseees,0,1440,charles_de_gaulle_etoile,5,120).
localTuristico(the_eiffel_tower,570,1485,bir_hakeim,5,300).

%lines

line(ligne1,[la_defense,esplanade_de_la_defense,pont_de_neuilly,les_sablons,porte_maillot,argentine,charles_de_gaulle_etoile,george_v,franklin_d_roosevelt,champs_elysees_clemenceau,concorde,tuileries,palais_royal_musee_du_louvre,louvre_rivoli,chatelet,hotel_de_ville,saint_paul,bastille,gare_de_lyon,reuilly_diderot,nation,porte_de_vincennes,saint_mande_tourelle,berault,chateau_de_vincennes]).

line(ligne2,[porte_dauphine,victor_hugo,charles_de_gaulle_etoile,ternes,coucerlles,monceau,villiers,rome,place_de_clichy,blanche,pigalle,anvers,barbes_rochechourt,la_chapelle,stalingrad,jaures,colonel_fabien,belleville,couronnes,menilmontant,pere_lachaise,philippe_auguste,alexandre_dumas,avron,nation]).

line(ligne3,[pont_de_levallois_becon,anatole_france,louise_michel,porte_de_champerret,pereire,wagram,malesherbes,villiers,europe,saint_lazare,havre_caumartin,opera,quatre_septembre,bourse,sentier,reaumur_sebastopol,arts_et_metiers,temple,republique,parmentier,rue_saint_maur,pere_lachaise,gambetta,porte_de_bagnolet,gallieni]).

line(ligne3bis,[gambetta,pelleport,saint_fargeau,porte_des_lilas]).

line(ligne4,[porte_de_clignancourt,simplon,marcadet_poissonniers,chateau_rouge,barbes_rochechouart,gare_du_nord,gare_de_l_est,chateau_d_eau,strasbourg_saint_denis,reaumur_sebastopol,etienne_marcel,les_halles,chatelet,cite,saint_michel,odeon,saint_germain_des_pres,saint_sulpice,saint_placide,montparnasse_bienvenue,vavin,raspail,denfert_rochereau,mouton_duvernet,alesia,porte_d_orleans,mairie_de_montrouge]).

line(ligne5,[bobigny_pablo_picasso,bobigny_pantin_raymond_queneau,eglise_de_pantin,hoche,porte_de_pantin,ourcq,laumiere,jaures,stalingrad,gare_du_nord,gare_de_l_est,jacques_bonsergent,republique,oberkampf,richard_lenoir,breguet_sabin,bastille,quai_de_la_rapee,gare_d_austerlitz,saint_marcel,campo_formio,place_d_italie]).

line(ligne6,[charles_de_gaulle_etoile,kleber,boissiere,trocadero,passy,bir_hakeim,dupleix,la_motte_oicquet_grenelle,cambronne,sevres_lecourbe,pasteur,montparnasse_bienvenue,edgar_quinet,raspail,denfert_rochereau,saint_jacques,glaciere,corvisart,place_d_italie,nationale,chevaleret,quai_de_la_gare,bercy,dugommier,daumensil,bel_air,picpus,nation]).

line(ligne7,[la_courneuve_8_mai_1945,fort_d_aubervillers,aubervilliers_pantin_quadre_chemins,porte_de_la_villette,corentin_cariou,crimee,riquet,stalingrad,louis_blanc,chateau_landon,gare_de_l_est,poissonniere,cadet,le_peletier,chaussee_d_antin_la_fayette,opera,pyramides,palais_royal_musee_du_louvre,pont_neuf,chatelet,pont_marie,sully_morland,jussieu,place_monge,censier_daubenton,les_gobelins,place_d_italie,tolbiac,maison_blanche,le_kremlin_bicetre,villejuif_leo_lagrange,villejuif_paul_vaillant_couturier,villejuif_lois_aragon]).

line(ligne7b,[maison_blanche,porte_d_italie,porte_de_choisy,porte_d_ivry,pierre_et_marie_curie,mairie_d_ivry]).

line(ligne7bis,[louis_blanc,jaures,bolivar,buttes_chaumont,botzaris,place_des_fetes,danube,pre_saint_gervais]).

line(ligne8,[balard,lourmel,boucicaut,felix_faure,commerce,la_motte_picquet_grenelle,ecole_militaire,la_tour_maubourg,invalides,concorde,madeleine,opera,richelieu_drouot,grands_boulevards,bonne_nouvelle,strasbourg_saint_denis,republique,filles_du_calvaire,saint_sebastien_froissart,chemin_vert,bastille,ledru_rollin,faidherbe_chaligny,reuilly_diderot,montgallet,daumesnil,michel_bizont,porte_doree,porte_de_charenton,liberte,charenton_ecoles,ecole_veterinaire_de_maisons_alfort,maisons_alfort_stade,maison_alfort_les_juilliottes,creteil_l_echat,creteil_universite,creteil_prefecture,pointe_du_lac]).

line(ligne9,[pont_de_sevres,billancourt,marcel_sembat,porte_de_saint_cloud,exelmans,michel_ange_molitor,michel_ange_auteuil,jasmin,ranelagh,la_muette,rue_de_la_pompe,trocadero,lena,alma_marceau,franklin_d_roosevelt,saint_philippe_du_roule,miromesnil,saint_augustin,havre_caumartin,chaussee_d_antin_la_fayette,richelieu_drouot,grands_boulevards,bonne_nouvelle,strasbourg_saint_denis,republique,oberkampf,saint_ambroise,voltaire,charonne,rue_des_boulets,nation,buzenval,maraichers,porte_de_montreuil,robespierre,croix_de_chavaux,mairie_de_montreuil]).

line(ligne10,[boulogne_pont_de_saint_cloud,boulogne_jean_jaures,porte_d_auteuil,michel_ange_molitor,michel_ange_auteuil,chardon_lagache,mirabeau,eglise_d_auteuil,javel_andre_citroen,charles_michels,avenue_emile_zola,la_motte_picquet_grenelle,segur,duroc,vaneau,sevres_babylone,mabillon,odeon,cluny_la_sorbonne,maubert_mutualite,cardinal_lemoine,jussieu,gare_d_austerlitz]).

line(ligne11,[chatelet,hotel_de_ville,rambuteau,arts_et_metiers,republique,goncourt,belleville,pyrenees,jourdain,place_des_fetes,telegraphe,porte_des_lilas,mairie_des_lilas]).

line(ligne12,[front_populaire,porte_de_la_chapelle,marx_dormoy,marcadet_poissonniers,jules_joffrin,lamarck_caulaincourt,abbesses,pigalle,saint_georges,notre_dame_de_lorette,trinite_d_estienne_d_orves,saint_lazare,madeleine,concorde,assemblee_nationale,solferino,rue_du_bac,sevres_babylone,rennes,notre_dame_des_champs,montparnasse_bienvenue,falguiere,pasteur,volontaires,vaugirard,convention,porte_de_versailles,corentin_celton,mairie_d_lssy]).

line(ligne13,[saint_denis_universite,bastilique_de_saint_denis,saint_denis_porte_de_paris,carrefour_pleyel,mairie_de_saint_ouen,garibaldi,porte_de_saint_ouen,guy_moquet,la_fourche,place_de_clichy,liege,saint_lazare,miromesnil,champs_elyseees_clemenceau,invalides,varenne,saint_francois_xavier,duroc,montparnasse_bienvenue,gaite,pernety,plaisance,porte_de_vanves,malakoff_plateau_de_vanves,malakoff_rue_etienne_dolet,chatillon_montrouge]).

line(ligne13b,[les_courtilles,les_agnettes,gabriel_peri,mairie_de_clichy,porte_de_clichy,brochant,la_fourche]).

line(ligne14,[saint_lazare,madeleine,pyramides,chatelet,gare_de_lyon,bercy,cour_saint_emilion,bibliotheque_francois_mitterrand,olympiades]).

%tempo de viagens entre estacoes

%line 1

liga(la_defense,esplanade_de_la_defense,ligne1,2).
liga(esplanade_de_la_defense,pont_de_neuilly,ligne1,2).
liga(pont_de_neuilly,les_sablons,ligne1,2).
liga(les_sablons,porte_maillot,ligne1,2).
liga(porte_maillot,argentine,ligne1,2).
liga(argentine,charles_de_gaulle_etoile,ligne1,2).
liga(charles_de_gaulle_etoile,george_v,ligne1,2).
liga(george_v,franklin_d_roosevelt,ligne1,2).
liga(franklin_d_roosevelt,champs_elysees_clemenceau,ligne1,2).
liga(champs_elysees_clemenceau,concorde,ligne1,2).
liga(concorde,tuileries,ligne1,2).
liga(tuileries,palais_royal_musee_du_louvre,ligne1,2).
liga(palais_royal_musee_du_louvre,louvre_rivoli,ligne1,2).
liga(louvre_rivoli,chatelet,ligne1,2).
liga(chatelet,hotel_de_ville,ligne1,2).
liga(hotel_de_ville,saint_paul,ligne1,2).
liga(saint_paul,bastille,ligne1,2).
liga(bastille,gare_de_lyon,ligne1,2).
liga(gare_de_lyon,reuilly_diderot,ligne1,2).
liga(reuilly_diderot,nation,ligne1,2).
liga(nation,porte_de_vincennes,ligne1,2).
liga(porte_de_vincennes,saint_mande_tourelle,ligne1,2).
liga(saint_mande_tourelle,berault,ligne1,2).
liga(berault,chateau_de_vincennes,ligne1,2).




%line 2

liga(porte_dauphine,victor_hugo,ligne2,2).
liga(victor_hugo,charles_de_gaulle_etoile,ligne2,2).
liga(charles_de_gaulle_etoile,ternes,ligne2,2).
liga(ternes,coucerlles,ligne2,2).
liga(coucerlles,monceau,ligne2,2).
liga(monceau,villiers,ligne2,2).
liga(villiers,rome,ligne2,2).
liga(rome,place_de_clichy,ligne2,2).
liga(place_de_clichy,blanche,ligne2,2).
liga(blanche,pigalle,ligne2,2).
liga(pigalle,anvers,ligne2,2).
liga(anvers,barbes_rochechourt,ligne2,2).
liga(barbes_rochechourt,la_chapelle,ligne2,2).
liga(la_chapelle,stalingrad,ligne2,2).
liga(stalingrad,jaures,ligne2,2).
liga(jaures,colonel_fabien,ligne2,2).
liga(colonel_fabien,belleville,ligne2,2).
liga(belleville,couronnes,ligne2,2).
liga(couronnes,menilmontant,ligne2,2).
liga(menilmontant,pere_lachaise,ligne2,2).
liga(pere_lachaise,philippe_auguste,ligne2,2).
liga(philippe_auguste,alexandre_dumas,ligne2,2).
liga(alexandre_dumas,avron,ligne2,2).
liga(avron,nation,ligne2,2).


%line 3


liga(pont_de_levallois_becon,anatole_france,ligne3,2).
liga(anatole_france,louise_michel,ligne3,2).
liga(louise_michel,porte_de_champerret,ligne3,2).
liga(porte_de_champerret,pereire,ligne3,2).
liga(pereire,wagram,ligne3,2).
liga(wagram,malesherbes,ligne3,2).
liga(malesherbes,villiers,ligne3,2).
liga(villiers,europe,ligne3,2).
liga(europe,saint_lazare,ligne3,2).
liga(saint_lazare,havre_caumartin,ligne3,2).
liga(havre_caumartin,opera,ligne3,2).
liga(opera,quatre_septembre,ligne3,2).
liga(quatre_septembre,bourse,ligne3,2).
liga(bourse,sentier,ligne3,2).
liga(sentier,reaumur_sebastopol,ligne3,2).
liga(reaumur_sebastopol,arts_et_metiers,ligne3,2).
liga(arts_et_metiers,temple,ligne3,2).
liga(temple,republique,ligne3,2).
liga(republique,parmentier,ligne3,2).
liga(parmentier,rue_saint_maur,ligne3,2).
liga(rue_saint_maur,pere_lachaise,ligne3,2).
liga(pere_lachaise,gambetta,ligne3,2).
liga(gambetta,porte_de_bagnolet,ligne3,2).
liga(porte_de_bagnolet,gallieni,ligne3,2).


%line 3bis

liga(gambetta,pelleport,ligne3bis,2).
liga(pelleport,saint_fargeau,ligne3bis,2).
liga(saint_fargeau,porte_des_lilas,ligne3bis,2).


%line 4

liga(porte_de_clignancourt,simplon,ligne4,2).
liga(simplon,marcadet_poissonniers,ligne4,2).
liga(marcadet_poissonniers,chateau_rouge,ligne4,2).
liga(chateau_rouge,barbes_rochechouart,ligne4,2).
liga(barbes_rochechouart,gare_du_nord,ligne4,2).
liga(gare_du_nord,gare_de_l_est,ligne4,2).
liga(gare_de_l_est,chateau_d_eau,ligne4,2).
liga(chateau_d_eau,strasbourg_saint_denis,ligne4,2).
liga(strasbourg_saint_denis,reaumur_sebastopol,ligne4,2).
liga(reaumur_sebastopol,etienne_marcel,ligne4,2).
liga(etienne_marcel,les_halles,ligne4,2).
liga(les_halles,chatelet,ligne4,2).
liga(chatelet,cite,ligne4,2).
liga(cite,saint_michel,ligne4,2).
liga(saint_michel,odeon,ligne4,2).
liga(odeon,saint_germain_des_pres,ligne4,2).
liga(saint_germain_des_pres,saint_sulpice,ligne4,2).
liga(saint_sulpice,saint_placide,ligne4,2).
liga(saint_placide,montparnasse_bienvenue,ligne4,2).
liga(montparnasse_bienvenue,vavin,ligne4,2).
liga(vavin,raspail,ligne4,2).
liga(raspail,denfert_rochereau,ligne4,2).
liga(denfert_rochereau,mouton_duvernet,ligne4,2).
liga(mouton_duvernet,alesia,ligne4,2).
liga(alesia,porte_d_orleans,ligne4,2).
liga(porte_d_orleans,mairie_de_montrouge,ligne4,2).


%line 5

liga(bobigny_pablo_picasso,bobigny_pantin_raymond_queneau,ligne5,2).
liga(bobigny_pantin_raymond_queneau,eglise_de_pantin,ligne5,2).
liga(eglise_de_pantin,hoche,ligne5,2).
liga(hoche,porte_de_pantin,ligne5,2).
liga(porte_de_pantin,ourcq,ligne5,2).
liga(ourcq,laumiere,ligne5,2).
liga(laumiere,jaures,ligne5,2).
liga(jaures,stalingrad,ligne5,2).
liga(stalingrad,gare_du_nord,ligne5,2).
liga(gare_du_nord,gare_de_l_est,ligne5,2).
liga(gare_de_l_est,jacques_bonsergent,ligne5,2).
liga(jacques_bonsergent,republique,ligne5,2).
liga(republique,oberkampf,ligne5,2).
liga(oberkampf,richard_lenoir,ligne5,2).
liga(richard_lenoir,breguet_sabin,ligne5,2).
liga(breguet_sabin,bastille,ligne5,2).
liga(bastille,quai_de_la_rapee,ligne5,2).
liga(quai_de_la_rapee,gare_d_austerlitz,ligne5,2).
liga(gare_d_austerlitz,saint_marcel,ligne5,2).
liga(saint_marcel,campo_formio,ligne5,2).
liga(campo_formio,place_d_italie,ligne5,2).


%line 6

liga(charles_de_gaulle_etoile,kleber,ligne6,2).
liga(kleber,boissiere,ligne6,2).
liga(boissiere,trocadero,ligne6,2).
liga(trocadero,passy,ligne6,2).
liga(passy,bir_hakeim,ligne6,2).
liga(bir_hakeim,dupleix,ligne6,2).
liga(dupleix,la_motte_oicquet_grenelle,ligne6,2).
liga(la_motte_oicquet_grenelle,cambronne,ligne6,2).
liga(cambronne,sevres_lecourbe,ligne6,2).
liga(sevres_lecourbe,pasteur,ligne6,2).
liga(pasteur,montparnasse_bienvenue,ligne6,2).
liga(montparnasse_bienvenue,edgar_quinet,ligne6,2).
liga(edgar_quinet,raspail,ligne6,2).
liga(raspail,denfert_rochereau,ligne6,2).
liga(denfert_rochereau,saint_jacques,ligne6,2).
liga(saint_jacques,glaciere,ligne6,2).
liga(glaciere,corvisart,ligne6,2).
liga(corvisart,place_d_italie,ligne6,2).
liga(place_d_italie,nationale,ligne6,2).
liga(nationale,chevaleret,ligne6,2).
liga(chevaleret,quai_de_la_gare,ligne6,2).
liga(quai_de_la_gare,bercy,ligne6,2).
liga(bercy,dugommier,ligne6,2).
liga(dugommier,daumensil,ligne6,2).
liga(daumensil,bel_air,ligne6,2).
liga(bel_air,picpus,ligne6,2).
liga(picpus,nation,ligne6,2).


%line 7

liga(la_courneuve_8_mai_1945,fort_d_aubervillers,ligne7,2).
liga(fort_d_aubervillers,aubervilliers_pantin_quadre_chemins,ligne7,2).
liga(aubervilliers_pantin_quadre_chemins,porte_de_la_villette,ligne7,2).
liga(porte_de_la_villette,corentin_cariou,ligne7,2).
liga(corentin_cariou,crimee,ligne7,2).
liga(crimee,riquet,ligne7,2).
liga(riquet,stalingrad,ligne7,2).
liga(stalingrad,louis_blanc,ligne7,2).
liga(louis_blanc,chateau_landon,ligne7,2).
liga(chateau_landon,gare_de_l_est,ligne7,2).
liga(gare_de_l_est,poissonniere,ligne7,2).
liga(poissonniere,cadet,ligne7,2).
liga(cadet,le_peletier,ligne7,2).
liga(le_peletier,chaussee_d_antin_la_fayette,ligne7,2).
liga(chaussee_d_antin_la_fayette,opera,ligne7,2).
liga(opera,pyramides,ligne7,2).
liga(pyramides,palais_royal_musee_du_louvre,ligne7,2).
liga(palais_royal_musee_du_louvre,pont_neuf,ligne7,2).
liga(pont_neuf,chatelet,ligne7,2).
liga(chatelet,pont_marie,ligne7,2).
liga(pont_marie,sully_morland,ligne7,2).
liga(sully_morland,jussieu,ligne7,2).
liga(jussieu,place_monge,ligne7,2).
liga(place_monge,censier_daubenton,ligne7,2).
liga(censier_daubenton,les_gobelins,ligne7,2).
liga(les_gobelins,place_d_italie,ligne7,2).
liga(place_d_italie,tolbiac,ligne7,2).
liga(tolbiac,maison_blanche,ligne7,2).
liga(maison_blanche,le_kremlin_bicetre,ligne7,2).
liga(le_kremlin_bicetre,villejuif_leo_lagrange,ligne7,2).
liga(villejuif_leo_lagrange,villejuif_paul_vaillant_couturier,ligne7,2).
liga(villejuif_paul_vaillant_couturier,villejuif_lois_aragon,ligne7,2).



%line 7b

liga(louis_blanc,jaures,ligne7bis,2).
liga(jaures,bolivar,ligne7bis,2).
liga(bolivar,buttes_chaumont,ligne7bis,2).
liga(buttes_chaumont,botzaris,ligne7bis,2).
liga(botzaris,place_des_fetes,ligne7bis,2).
liga(place_des_fetes,danube,ligne7bis,2).
liga(danube,pre_saint_gervais,ligne7bis,2).



%line 8

liga(balard,lourmel,ligne8,2).
liga(lourmel,boucicaut,ligne8,2).
liga(boucicaut,felix_faure,ligne8,2).
liga(felix_faure,commerce,ligne8,2).
liga(commerce,la_motte_picquet_grenelle,ligne8,2).
liga(la_motte_picquet_grenelle,ecole_militaire,ligne8,2).
liga(ecole_militaire,la_tour_maubourg,ligne8,2).
liga(la_tour_maubourg,invalides,ligne8,2).
liga(invalides,concorde,ligne8,2).
liga(concorde,madeleine,ligne8,2).
liga(madeleine,opera,ligne8,2).
liga(opera,richelieu_drouot,ligne8,2).
liga(richelieu_drouot,grands_boulevards,ligne8,2).
liga(grands_boulevards,bonne_nouvelle,ligne8,2).
liga(bonne_nouvelle,strasbourg_saint_denis,ligne8,2).
liga(strasbourg_saint_denis,republique,ligne8,2).
liga(republique,filles_du_calvaire,ligne8,2).
liga(filles_du_calvaire,saint_sebastien_froissart,ligne8,2).
liga(saint_sebastien_froissart,chemin_vert,ligne8,2).
liga(chemin_vert,bastille,ligne8,2).
liga(bastille,ledru_rollin,ligne8,2).
liga(ledru_rollin,faidherbe_chaligny,ligne8,2).
liga(faidherbe_chaligny,reuilly_diderot,ligne8,2).
liga(reuilly_diderot,montgallet,ligne8,2).
liga(montgallet,daumesnil,ligne8,2).
liga(daumesnil,michel_bizont,ligne8,2).
liga(michel_bizont,porte_doree,ligne8,2).
liga(porte_doree,porte_de_charenton,ligne8,2).
liga(porte_de_charenton,liberte,ligne8,2).
liga(liberte,charenton_ecoles,ligne8,2).
liga(charenton_ecoles,ecole_veterinaire_de_maisons_alfort,ligne8,2).
liga(ecole_veterinaire_de_maisons_alfort,maisons_alfort_stade,ligne8,2).
liga(maisons_alfort_stade,maison_alfort_les_juilliottes,ligne8,2).
liga(maison_alfort_les_juilliottes,creteil_l_echat,ligne8,2).
liga(creteil_l_echat,creteil_universite,ligne8,2).
liga(creteil_universite,creteil_prefecture,ligne8,2).
liga(creteil_prefecture,pointe_du_lac,ligne8,2).


%line 9

liga(pont_de_sevres,billancourt,ligne9,2).
liga(billancourt,marcel_sembat,ligne9,2).
liga(marcel_sembat,porte_de_saint_cloud,ligne9,2).
liga(porte_de_saint_cloud,exelmans,ligne9,2).
liga(exelmans,michel_ange_molitor,ligne9,2).
liga(michel_ange_molitor,michel_ange_auteuil,ligne9,2).
liga(michel_ange_auteuil,jasmin,ligne9,2).
liga(jasmin,ranelagh,ligne9,2).
liga(ranelagh,la_muette,ligne9,2).
liga(la_muette,rue_de_la_pompe,ligne9,2).
liga(rue_de_la_pompe,trocadero,ligne9,2).
liga(trocadero,lena,ligne9,2).
liga(lena,alma_marceau,ligne9,2).
liga(alma_marceau,franklin_d_roosevelt,ligne9,2).
liga(franklin_d_roosevelt,saint_philippe_du_roule,ligne9,2).
liga(saint_philippe_du_roule,miromesnil,ligne9,2).
liga(miromesnil,saint_augustin,ligne9,2).
liga(saint_augustin,havre_caumartin,ligne9,2).
liga(havre_caumartin,chaussee_d_antin_la_fayette,ligne9,2).
liga(chaussee_d_antin_la_fayette,richelieu_drouot,ligne9,2).
liga(richelieu_drouot,grands_boulevards,ligne9,2).
liga(grands_boulevards,bonne_nouvelle,ligne9,2).
liga(bonne_nouvelle,strasbourg_saint_denis,ligne9,2).
liga(strasbourg_saint_denis,republique,ligne9,2).
liga(republique,oberkampf,ligne9,2).
liga(oberkampf,saint_ambroise,ligne9,2).
liga(saint_ambroise,voltaire,ligne9,2).
liga(voltaire,charonne,ligne9,2).
liga(charonne,rue_des_boulets,ligne9,2).
liga(rue_des_boulets,nation,ligne9,2).
liga(nation,buzenval,ligne9,2).
liga(buzenval,maraichers,ligne9,2).
liga(maraichers,porte_de_montreuil,ligne9,2).
liga(porte_de_montreuil,robespierre,ligne9,2).
liga(robespierre,croix_de_chavaux,ligne9,2).
liga(croix_de_chavaux,mairie_de_montreuil,ligne9,2).




%line 10

liga(boulogne_pont_de_saint_cloud,boulogne_jean_jaures,ligne10,2).
liga(boulogne_jean_jaures,porte_d_auteuil,ligne10,2).
liga(porte_d_auteuil,michel_ange_molitor,ligne10,2).
liga(michel_ange_molitor,michel_ange_auteuil,ligne10,2).
liga(michel_ange_auteuil,chardon_lagache,ligne10,2).
liga(chardon_lagache,mirabeau,ligne10,2).
liga(mirabeau,eglise_d_auteuil,ligne10,2).
liga(eglise_d_auteuil,javel_andre_citroen,ligne10,2).
liga(javel_andre_citroen,charles_michels,ligne10,2).
liga(charles_michels,avenue_emile_zola,ligne10,2).
liga(avenue_emile_zola,la_motte_picquet_grenelle,ligne10,2).
liga(la_motte_picquet_grenelle,segur,ligne10,2).
liga(segur,duroc,ligne10,2).
liga(duroc,vaneau,ligne10,2).
liga(vaneau,sevres_babylone,ligne10,2).
liga(sevres_babylone,mabillon,ligne10,2).
liga(mabillon,odeon,ligne10,2).
liga(odeon,cluny_la_sorbonne,ligne10,2).
liga(cluny_la_sorbonne,maubert_mutualite,ligne10,2).
liga(maubert_mutualite,cardinal_lemoine,ligne10,2).
liga(cardinal_lemoine,jussieu,ligne10,2).
liga(jussieu,gare_d_austerlitz,ligne10,2).



%line 11

liga(chatelet,hotel_de_ville,ligne11,2).
liga(hotel_de_ville,rambuteau,ligne11,2).
liga(rambuteau,arts_et_metiers,ligne11,2).
liga(arts_et_metiers,republique,ligne11,2).
liga(republique,goncourt,ligne11,2).
liga(goncourt,belleville,ligne11,2).
liga(belleville,pyrenees,ligne11,2).
liga(pyrenees,jourdain,ligne11,2).
liga(jourdain,place_des_fetes,ligne11,2).
liga(place_des_fetes,telegraphe,ligne11,2).
liga(telegraphe,porte_des_lilas,ligne11,2).
liga(porte_des_lilas,mairie_des_lilas,ligne11,2).


%line 12

liga(front_populaire,porte_de_la_chapelle,ligne12,2).
liga(porte_de_la_chapelle,marx_dormoy,ligne12,2).
liga(marx_dormoy,marcadet_poissonniers,ligne12,2).
liga(marcadet_poissonniers,jules_joffrin,ligne12,2).
liga(jules_joffrin,lamarck_caulaincourt,ligne12,2).
liga(lamarck_caulaincourt,abbesses,ligne12,2).
liga(abbesses,pigalle,ligne12,2).
liga(pigalle,saint_georges,ligne12,2).
liga(saint_georges,notre_dame_de_lorette,ligne12,2).
liga(notre_dame_de_lorette,trinite_d_estienne_d_orves,ligne12,2).
liga(trinite_d_estienne_d_orves,saint_lazare,ligne12,2).
liga(saint_lazare,madeleine,ligne12,2).
liga(madeleine,concorde,ligne12,2).
liga(concorde,assemblee_nationale,ligne12,2).
liga(assemblee_nationale,solferino,ligne12,2).
liga(solferino,rue_du_bac,ligne12,2).
liga(rue_du_bac,sevres_babylone,ligne12,2).
liga(sevres_babylone,rennes,ligne12,2).
liga(rennes,notre_dame_des_champs,ligne12,2).
liga(notre_dame_des_champs,montparnasse_bienvenue,ligne12,2).
liga(montparnasse_bienvenue,falguiere,ligne12,2).
liga(falguiere,pasteur,ligne12,2).
liga(pasteur,volontaires,ligne12,2).
liga(volontaires,vaugirard,ligne12,2).
liga(vaugirard,convention,ligne12,2).
liga(convention,porte_de_versailles,ligne12,2).
liga(porte_de_versailles,corentin_celton,ligne12,2).
liga(corentin_celton,mairie_d_lssy,ligne12,2).


%line 13

liga(saint_denis_universite,bastilique_de_saint_denis,ligne13,2).
liga(bastilique_de_saint_denis,saint_denis_porte_de_paris,ligne13,2).
liga(saint_denis_porte_de_paris,carrefour_pleyel,ligne13,2).
liga(carrefour_pleyel,mairie_de_saint_ouen,ligne13,2).
liga(mairie_de_saint_ouen,garibaldi,ligne13,2).
liga(garibaldi,porte_de_saint_ouen,ligne13,2).
liga(porte_de_saint_ouen,guy_moquet,ligne13,2).
liga(guy_moquet,la_fourche,ligne13,2).
liga(la_fourche,place_de_clichy,ligne13,2).
liga(place_de_clichy,liege,ligne13,2).
liga(liege,saint_lazare,ligne13,2).
liga(saint_lazare,miromesnil,ligne13,2).
liga(miromesnil,champs_elyseees_clemenceau,ligne13,2).
liga(champs_elyseees_clemenceau,invalides,ligne13,2).
liga(invalides,varenne,ligne13,2).
liga(varenne,saint_francois_xavier,ligne13,2).
liga(saint_francois_xavier,duroc,ligne13,2).
liga(duroc,montparnasse_bienvenue,ligne13,2).
liga(montparnasse_bienvenue,gaite,ligne13,2).
liga(gaite,pernety,ligne13,2).
liga(pernety,plaisance,ligne13,2).
liga(plaisance,porte_de_vanves,ligne13,2).
liga(porte_de_vanves,malakoff_plateau_de_vanves,ligne13,2).
liga(malakoff_plateau_de_vanves,malakoff_rue_etienne_dolet,ligne13,2).
liga(malakoff_rue_etienne_dolet,chatillon_montrouge,ligne13,2).

%line 13b

liga(les_courtilles,les_agnettes,ligne13b,2).
liga(les_agnettes,gabriel_peri,ligne13b,2).
liga(gabriel_peri,mairie_de_clichy,ligne13b,2).
liga(mairie_de_clichy,porte_de_clichy,ligne13b,2).
liga(porte_de_clichy,brochant,ligne13b,2).
liga(brochant,la_fourche,ligne13b,2).


%line 14

liga(saint_lazare,madeleine,ligne14,2).
liga(madeleine,pyramides,ligne14,2).
liga(pyramides,chatelet,ligne14,2).
liga(chatelet,gare_de_lyon,ligne14,2).
liga(gare_de_lyon,bercy,ligne14,2).
liga(bercy,cour_saint_emilion,ligne14,2).
liga(cour_saint_emilion,bibliotheque_francois_mitterrand,ligne14,2).
liga(bibliotheque_francois_mitterrand,olympiades,ligne14,2).



%generate connections between two stations dynamically 
%print2stations([_]).
%print2stations([S1,S2|T]):-write('liga('),write(S1),write(','),write(S2),write(','),write(2),write(').'),nl,print2stations([S2|T]).

%liga(_,_,2).








%
primeiroMetro(332).
ultimoMetro(diaNormal,1515).
ultimoMetro(sextaSabadoVesparaFeriado,1675).

frequencia(332,539,2).
frequencia(540,899,4).
frequencia(900,1199,2).
frequencia(1200,1515,7).

paragemEstacao(1).


%nome, horarioA, horarioF, tVisita
pontoTuristico(pt1,300,700,60).
pontoTuristico(pt2,300,700,90).

%ptT, estacao, distancia
perto(pt1, bercy,7).
perto(pt2, victor_hugo,10).
