:- include('data.pl').


split_string(String,Collected_strings):-
        string_to_list(String,Charlist),
        char_code(':',SemiColon),
        collect_strings(Charlist,SemiColon,Collected_strings).
collect_strings([],_,[]):-!.
collect_strings(Charlist,Last,[String|Collected_strings]):-
        collect_chars(Charlist,Nextlist,Last,Collected_chars),
        string_to_list(String,Collected_chars),
        collect_strings(Nextlist,Last,Collected_strings).
collect_chars([],_,_,[]):-!.


collect_chars([58|Charlist],Charlist,Last,[]):-
        Last\==58,!.
collect_chars([58|Charlist],_,58,Collected_chars):-
        collect_chars(Charlist,_,58,Collected_chars),!.
collect_chars([Code|Charlist],Nextlist,_,[Code|Collected_chars]):-
        Code\==58,
        Last1=Code,
        collect_chars(Charlist,Nextlist,Last1,Collected_chars),!.

        
%%%no works, dunno how to convert string to integer        
%minutesToHours(String,[H,M],TotalTime):-split_string(String,[H,M]),write('Hours '),write(H), write(' minute '),write(M),number_codes(N, H)

%TotalTime is (H*60),write(TotalTime).


%%% converts number of minutes to a valid time-string e.g. '03:05':

mins2hourmin(MINS,OUTX):- nonvar(MINS), MINS > 0,
    Hours is MINS // 60, Minsx is MINS mod 60,
    num2str2(Hours,S1x), num2str2(Minsx,S2x),
    swritef(OUTX,'%w:%w',[S1x,S2x]), !
    ;
    MINS = 0, OUTX = '00:00', !.
mins2hourmin(HMINx,HRi):- nonvar(HRi),
    sub_atom(HRi,0,2,_,A1x), atom_number(A1x,HOURx),
    sub_atom(HRi,3,2,_,A2x), atom_number(A2x,MINSx),
    HMINx is MINSx + 60*HOURx, !.
mins2hourmin(MINS1,OUTX):- nonvar(MINS1), MINS1 < 0,
    MINS is -MINS1,
    Hours is MINS // 60, Minsx is MINS mod 60,
    num2str2(Hours,S1x), num2str2(Minsx,S2x),
    swritef(OUTX,'-%w:%w',[S1x,S2x]), !.

    %%% an auxilliary predicate for mins2hourmin/2:
num2str2(N,Sx):- N >= 10, swritef(Sx,'%w',[N]), !
    ;
    swritef(Sx,'0%w',[N]), !.




%prints a formatted output
print([]).
print([H|T]):-format('from ~w take ~w to ~w\n', H),print(T).
%print times
printTimes(T,W):- Tot is T+W, write('Total time: '),write(Tot),write(', travelling: '),write(T),write(', walking: '),write(W),nl.
printConnections(N):- write('Number of connections: '),write(N),nl.

getTravTime([[E1,L,E2]],T,0):-connect(E1,E2,L,T,[E1]).
getTravTime([[E1,L,E2]|R],T,W):-connect(E1,E2,L,T1,[E1]),getTravTime(R,T2,TP),paragemEstacao(Tm), T is T2+T1, W is TP+Tm.



%3------------------------------------------------------------------------------------------------------------------------
%shortest/fastest Path (T/W/N)-time,walking,connections
%fastest(Orig,Dest):

fastest(Origem,Destino):-
			branchAndBound2(Destino,[(0,[Origem])],Solucao,Custo),writeBfs(Solucao),write('Duration: '),write(Custo),write(' minutes.').


branchAndBound2(Destino,[(Custo,[Destino|T])|_],Solucao,Custo):-
					reverse([Destino|T],Solucao).

branchAndBound2(Destino,[(Custo,[H|T])|Resto],Solucao,CustoSol):-
			findall((C,[X,H|T]),(Destino\==H,findRoute(H,X,[],[],W,TW,_),\+ member(X,[H|T]),C is W+TW+Custo),Novos),
			append(Novos,Resto,Todos),
			sort(Todos,LS),
			%write(LS),nl,
			branchAndBound2(Destino,LS,Solucao,CustoSol).


leastConnections(Orig,Dest):-bfs2(Dest,[[Orig]],Caminho),writeBfs(Caminho).

bfs2(Dest,[[Dest|T]|_],Cam):-%inverte o caminho actual
reverse([Dest|T],Cam).

bfs2(Dest,[[H|T]|Outros],Cam):-findall([X,H|T],(Dest\==H,findRoute(H,X),not(member(X,[H|T]))),Novos),
	append(Outros,Novos,Todos),
	%write(Todos),nl,
	bfs2(Dest,Todos,Cam).


writeBfs([_]).
writeBfs([H1,H2|T]):-findWritingRoute(H1,H2,[],[]),writeBfs([H2|T]).


%least walking
leastWalking(Origem,Destino):-
			branchAndBound1(Destino,[(0,[Origem])],Solucao,Custo),writeBfs(Solucao),write('Walking time: '),write(Custo),write(' minutes.').


branchAndBound1(Destino,[(Custo,[Destino|T])|_],Solucao,Custo):-write('base'),
					reverse([Destino|T],Solucao).

branchAndBound1(Destino,[(Custo,[H|T])|Resto],Solucao,CustoSol):-
			findall((C,[X,H|T]),(Destino\==H,findRoute(H,X,[],[],_,TW,_),\+ member(X,[H|T]),C is TW+Custo),Novos),%write('base1'),
			append(Novos,Resto,Todos),%write('append'),
			sort(Todos,LS),%write('sort'),
			%write(LS),nl,
			branchAndBound1(Destino,LS,Solucao,CustoSol),write('recursion').


			
			

findWritingRoute(X,Y,Lines,Output):-line(Line,Stations),\+ member(Line,Lines),member(X,Stations),member(Y,Stations),
				    append(Output,[ [X,Line,Y]],NewOutput),print(NewOutput).

findRoute(X,Y):-findRoute(X,Y,[],[],_,_,_).
findRoute(X,Y,Z):-findRoute(X,Y,[],[],_,_,Z).



%%%%%%%%%%%%%%% DIRECT ROUTE %%%%%%%%%%%%%%
%Firstly we consider a route from X to Y not using any of the Lines specified in
%Lines and using only a sinlge line

%write direct output.
%T- travel time
%W- walk time
findRoute(X,Y,Lines,Output,T,W,Ret):-line(Line,Stations),\+ member(Line,Lines),member(X,Stations),member(Y,Stations),
				     append(Output,[ [X,Line,Y]],NewOutput),
				     getTravTime(NewOutput,T,W), Ret =NewOutput.


%%%%%%%%%%%%%% INDIRECT ROUTE %%%%%%%%%%%%%
%Next we consider an indirect route from X to Y not using any of the routes
%specified in Lines via an intermediate station Intermediate
findRoute(X,Y,Lines,Output,T,W,Ret):-line(Line,Stations),\+ member(Line,Lines),member(X,Stations),member(Intermediate,Stations),X\=Intermediate,Intermediate\=Y,
				     append(Output,[[X,Line,Intermediate]],NewOutput),
				     findRoute(Intermediate,Y,[Line|Lines],NewOutput,T,W,Ret).



%We use a V list which has all the visited stations (previously we had a stack overflow problem, since we didnt store which stations have been visited)
connect(E1,E2,L,T,_):-(liga(E1,E2,L,T);liga(E2,E1,L,T)).

connect(E1,E2,L,T,V):-(liga(E1,E3,L,T1);liga(E3,E1,L,T1)),not(member(E3,V)),connect(E3,E2,L,T2,[E3|V]),T is T1+T2.


%5----------------------------------------------------------------------
tour(L,PAct,V):-((V=meio,tour(L,PAct,300,0,[],Cam));
		 tour(L,PAct,480,0,[],Cam)),!,printTour(Cam).

tour([P],PAct,TLim,TAct,R,Cam):-travel(PAct,P,TViagem,Res),append([Res,P,PAct],R,Novo), T1 is TAct+TViagem, T1=<TLim,reverse(Novo,Cam).

tour([H|T],PAct,TLim,TAct,R,Cam):-(TAct<TLim, travel(PAct,H,TViagem,Res),append([Res,H,PAct],R,Novo),T1 is TAct+TViagem,tour(T,H,TLim,T1,Novo,Cam));
				  (!,write('Not enough time!!'),nl).

travel(O,D,T,Cam):-(perto(D,O,T), Cam=[]);
		   (perto(O,E1,T1),perto(D,E2,T2),findRoute(E1,E2,[],[],TV,TW,Cam),pontoTuristico(D,_,_,Vis),T is T1+T2+TV+TW+Vis);
		   (perto(D,E2,T2),connect(O,_,_,_,_),findRoute(O,E2,[],[],TV,TW,Cam),pontoTuristico(D,_,_,Vis),T is T2+TV+TW+Vis).

printTour([]).
printTour([Pt1,Pt2,Cam|T]):- write('Go from '),write(Pt1),write(' to '), write(Pt2),write('.'),nl,print(Cam),nl,nl,printTour(T). 


%6-------------------------------------------------------------------------
cycleTour(L,PAct,Hini,V):-((V=meio, append(L,[PAct],List),cycleTour(List,PAct,Hini,300,0,[],Cam));
			   append(L,[PAct],List),cycleTour(List,PAct,Hini,480,0,[],Cam)),!,fileCycle(Cam).

cycleTour([P],PAct,Hini,TLim,TAct,R,Cam):-travelTime(PAct,P,TAct+Hini,TViagem,Res),T1 is TAct+TViagem,HAct is Hini+T1,
					  append([Res,P,PAct,HAct],R,Novo), T1=<TLim,reverse(Novo,Cam).

cycleTour([H|T],PAct,Hini,TLim,TAct,R,Cam):-(TAct<TLim, travelTime(PAct,H,TAct+Hini,TViagem,Res),T1 is TAct+TViagem,HAct is Hini+T1,
					     append([Res,H,PAct,HAct],R,Novo),cycleTour(T,H,Hini,TLim,T1,Novo,Cam));
					    (!,write('Not enough time!!'),nl).


travelTime(O,D,TAct,T,Cam):-(perto(D,O,T), Cam=[]);
			    (perto(O,E1,T1),perto(D,E2,T2),findRoute(E1,E2,[],[],TV,TW,Cam),pontoTuristico(D,Ha,Hf,Vis),TAct>=Ha,TAct+Vis=<Hf,T is T1+T2+TV+TW+Vis);
			    (perto(O,E1,T1),connect(D,_,_,_,_),findRoute(E1,D,[],[],TV,TW,Cam),T is T1+TV+TW);
			    (perto(D,E2,T2),connect(O,_,_,_,_),findRoute(O,E2,[],[],TV,TW,Cam),pontoTuristico(D,Ha,Hf,Vis),TAct>=Ha,TAct+Vis=<Hf,T is T2+TV+TW+Vis).


fileCycle(L):-open('output.txt', write, OS), printCycleTour(L,OS),close(OS).

printCycleTour([],_).
printCycleTour([Time,Pt1,Pt2,Cam|T],OS):- write(OS,'Go from '),write(OS,Pt1),write(OS,' to '), write(OS,Pt2),write(OS,'.'),nl(OS),filePrint(Cam,OS),write(OS,'Time: '),write(OS,Time),nl(OS),nl(OS),printCycleTour(T,OS). 

%prints a formatted output
filePrint([],_).
filePrint([[E1,L,E2]|T],OS):-write(OS,'From'),write(OS,E1),write(OS,' take '),write(OS,L), write(OS,' to '), write(OS,E2),nl(OS),filePrint(T,OS).
