%estacaoMenosTrocas(E1,E2,L,T,P).

%estacaoMaisRapido(E1,E2,L,T,P).

%estacaoMenorPe(E1,E2,L,T,P).
%adjacent(estacao1,estacao2,linha, tempoViagem).
adjacent(e1,e2,l1,1).
adjacent(e2,e3,l1,2).
adjacent(e3,e4,l1,2).
adjacent(e2,e5,l2,1).
adjacent(e5,e6,l2,2).
adjacent(e6,e7,l3,7).
%linha(1,[e1,e2,e3],5,10,10,15).
%linha(2,[e1,e4,e5],5,10,10,15).
%linha(3,[e1,e3,e4],5,10,10,15).
%linha(4,[e2,e5],5,10,10,15).

tPe(2).

next(X,Y,L):-adjacent(X,Y,L,_).
next(X,Y,L):-adjacent(Y,X,L,_).

nextTime(X,Y,L,T):-adjacent(X,Y,L,T).
nextTime(X,Y,L,T):-adjacent(Y,X,L,T).


direct_connect(X,Y,L,S,F):-
                next(X,Z,L),
                not(member(Z,S)),
                direct_connect(Z,Y,L,[Z|S],F).

direct_connect(X,Y,L,S,[Y|S]):- next(X,Y,L).

%uma unica troca de linha
one_change(X,Y,L,F):-
                direct_connect(X,Z,L,[X],F1),
                direct_connect(Z,Y,L2,[Z|F1],F),
                L\=L2.

%n trocas de linha.
n_changes(X,Y,L,F,1):-
	direct_connect(X,Z,L,[X],F1),
        direct_connect(Z,Y,L2,[Z|F1],F),L\=L2.

n_changes(X,Y,L,F,N):-
	direct_connect(X,Z,L,[X],_),
	n_changes(Z,Y,L2,F2,N1),
	L\=L2, N is N1+1,
	append(F2,[X],F).


%para uma estacao existir tem que ter outra adjacente.
exist(X):-next(X,_,_).


%planear rotas.
route(X,Y,F,T,0):-exist(X),exist(Y),
       direct_connect(X,Y,_,[X],F),
       write('Direct Connection'),nl,
       revwrite(F),
       getTravelTime(F,T).

route(X,Y,F,0,0):-exist(X),exist(Y),
              %one_change(X,Y,_,F),
              n_changes(X,Y,_,F,N),
	      write(N),
	      write(' changes required'),nl,
              revwrite(F),nl,nl.
	     % getTravelTime(F,T).
	      %tPe(Pe).
	      %TPe is Pe*N.

%escrever em ordem reversa.
revwrite([X]):-write(X).
revwrite([H|T]):-revwrite(T), write('->'),write(H).

%concatenar listas.
append([], List, List).
append([Head|Tail], List, [Head|Rest]) :-
    append(Tail, List, Rest).

%tempo de viagem entre estacoes.
getTravelTime([_],0).
getTravelTime([E1,E2|L],T):-
	getTravelTime([E2|L],T1),
	nextTime(E1,E2,_,TViagem),
	T is TViagem+T1.
