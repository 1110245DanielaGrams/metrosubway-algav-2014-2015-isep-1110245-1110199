# About #
A project about Paris metro in Prolog, a project for the ALGAV class of 2014/2015 at DEI, ISEP

## What it does ##
* Find a route between two stations.
* Find the shortest path using Branch and Bound.
* Find the path with less line changes using Breadth First Search.
* Visit touristic sites using the metro to get there.
* Make half day and full day routes between tour sites using visiting hours.
* Make a full route between tour sites returning to the starting point and output the results to a file

## Made by ##
Daniela Grams

Duarte Teles 