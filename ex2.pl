linha(1,[a,b,c,d,e,f]). 
linha(2,[g,b,h,i,j,k]). 
linha(3,[l,j,m,n,o,d,p]). 
linha(4,[f,q,r,s,t]). 
linha(5,[t,u,j,v,a,g]).
%linha(7,[f,a]).

%a
inter([], _, []).
inter([H1|T1], L2, [H1|Res]):- member(H1, L2), inter(T1, L2, Res).
inter([_|T1], L2, Res):- inter(T1, L2, Res).

:-dynamic cruza/3, estacoes/1, estacao_linhas/2.
gera_cruzamentos:-findall(_,cruzamento,_). 
cruzamento:- linha(N1,LE1), 
	 linha(N2,LE2),
	 (N1\==N2 , 
	 inter(LE1,LE2,LI),  
		(LI \==[],
		assertz(cruza(N1,N2,LI)))). 


uniao([ ],L,L).
uniao([X|L],L1,L2):- member(X,L1),!,uniao(L,L1,L2).
uniao([X|L],L1,[X|L2]):- uniao(L,L1,L2).

%b
%estacoes:- linha(N1,LE1), 
%	 linha(N2,LE2),
%	 (N1\==N2 , 
%	 uniao(LE1,LE2,LI),  
%		(LI \==[],
%		assertz(est(N1,N2,LI)))). 


%gera_estacoes:-findall(_,estacoes,_).

gera_estacoes:-findall(L,linha(_,L),LE),
	gera_estacoes(LE,Estacoes),
	assertz(estacoes(Estacoes)).

gera_estacoes([H|[]],H):-!.
gera_estacoes([H|T],U):-gera_estacoes(T,X),
	uniao(H,X,U).



%c
gera_estacoes_linhas:-gera_estacoes,estacoes(L),
	gera_estacoes_linhas(L).

gera_estacoes_linhas([]):-!.

gera_estacoes_linhas([H|L]):-findall(X,(linha(X,L1),
	member(H,L1)),LE),
	assertz(estacao_linha(H,LE)),
	gera_estacoes_linhas(L).


%d
%gera_caminhos(E1,E2,LE):-findall(X,
%	(cruza(X,_,L),
%		member(E1,L),
%		cruza(X,_,L1),
%		member(E2,L1)),
%	LE),!.

%gera_caminhos(E1,E2,LE):-findall(X,
%	(cruza(X,_,L),
%		member(E1,L),
%		cruza(X,_,L1),
%		member(E3,L1)),
%	LE),gera_caminhos(E3,E2,LE).
get_linha([H|_],H).

gera_caminhos(E1,E2,(E1,E2,F)):-findall(X,(linha(X,L),member(E1,L),member(E2,L)),EL),get_linha(EL,F).

gera_caminhos(E1,E2,[(E1,E3,EL1)|LE]):-estacao_linha(E1,L1),estacao_linha(E2,L2),get_linha(L1,EL1),get_linha(L2,EL2),cruza(EL1,EL2,EL),get_linha(EL,E3),gera_caminhos(E3,E2,LE).
%findall(X,(linha(X,L),member(E1,L),member(E3,L)),_),gera_caminhos(E3,E2,LE).


caminho(EO,ED,LT):-estacao_linha(EO,LEO),
	estacao_linha(ED,LED),
	caminho(EO,LEO,ED,LED,[],LT).

caminho(EO,LEO,ED,LED,_,[(EO,ED,Linha)]):- inter(LEO,LED,LC),
	member(Linha,LC),!.

caminho(EO,LEO,ED,LED,LVisitadas,[(EO,ET,Linha)|LFinal]):-member(Linha,LEO),
	(not(member(Linha,LVisitadas))),
	cruza(Linha,_,LT),
	member(ET,LT),
	estacao_linha(ET,LET),
	caminho(ET,LET,ED,LED,[Linha|LVisitadas],LFinal).



