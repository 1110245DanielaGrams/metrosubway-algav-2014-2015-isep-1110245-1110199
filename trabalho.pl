
%gera_cruzamentos: gera os cruzamentos entre as várias estações
%cruza(L1,L2,LE): linha1,linha2,lista de estacoes
inter([], _, []).
inter([H1|T1], L2, [H1|Res]):- member(H1, L2), inter(T1, L2, Res).
inter([_|T1], L2, Res):- inter(T1, L2, Res).

:-dynamic cruza/3, estacoes/1, estacao_linhas/2.
gera_cruzamentos:-findall(_,cruzamento,_). 
cruzamento:- linha(N1,LE1), 
	 linha(N2,LE2),
	 (N1\==N2 , 
	 inter(LE1,LE2,LI),  
		(LI \==[],
		assertz(cruza(N1,N2,LI)))). 


uniao([ ],L,L).
uniao([X|L],L1,L2):- member(X,L1),!,uniao(L,L1,L2).
uniao([X|L],L1,[X|L2]):- uniao(L,L1,L2).



%__________________________________________________________________
%lista todas as estações

%estacoes:- linha(N1,LE1), 
%	 linha(N2,LE2),
%	 (N1\==N2 , 
%	 uniao(LE1,LE2,LI),  
%		(LI \==[],
%		assertz(est(N1,N2,LI)))). 


%gera_estacoes:-findall(_,estacoes,_).

gera_estacoes:-findall(L,linha(_,L),LE),
	gera_estacoes(LE,Estacoes),
	assertz(estacoes(Estacoes)).

gera_estacoes([H|[]],H):-!.
gera_estacoes([H|T],U):-gera_estacoes(T,X),
	uniao(H,X,U).



%______________________________________________________________________
%estacao_linhas(estação,lista_de_todas_linhas_que_passam_pela_estação). 

gera_estacoes_linhas:-gera_estacoes,estacoes(L),
	gera_estacoes_linhas(L).

gera_estacoes_linhas([]):-!.

gera_estacoes_linhas([H|L]):-findall(X,(linha(X,L1),
	member(H,L1)),LE),
	assertz(estacao_linha(H,LE)),
	gera_estacoes_linhas(L).

%________________________________________________________________________
%gera_caminho(Eorigem, Edestino, Lcruzamentos)
%gera_caminhos(E1,E2,LE):-findall(X,
%	(cruza(X,_,L),
%		member(E1,L),
%		cruza(X,_,L1),
%		member(E2,L1)),
%	LE),!.

%gera_caminhos(E1,E2,LE):-findall(X,
%	(cruza(X,_,L),
%		member(E1,L),
%		cruza(X,_,L1),
%		member(E3,L1)),
%	LE),gera_caminhos(E3,E2,LE).
get_linha([H|_],H).

gera_caminhos(E1,E2,(E1,E2,F)):-findall(X,(linha(X,L),member(E1,L),member(E2,L)),EL),get_linha(EL,F).

gera_caminhos(E1,E2,[(E1,E3,EL1)|LE]):-estacao_linha(E1,L1),estacao_linha(E2,L2),get_linha(L1,EL1),get_linha(L2,EL2),cruza(EL1,EL2,EL),get_linha(EL,E3),gera_caminhos(E3,E2,LE).
%findall(X,(linha(X,L),member(E1,L),member(E3,L)),_),gera_caminhos(E3,E2,LE).

%___________________________________________________________________________________
%menor caminho??
caminho(EO,ED,LT):-estacao_linha(EO,LEO),
	estacao_linha(ED,LED),
	caminho(EO,LEO,ED,LED,[],LT).

caminho(EO,LEO,ED,LED,_,[(EO,ED,Linha)]):- inter(LEO,LED,LC),
	member(Linha,LC),!.

caminho(EO,LEO,ED,LED,LVisitadas,[(EO,ET,Linha)|LFinal]):-member(Linha,LEO),
	(not(member(Linha,LVisitadas))),
	cruza(Linha,_,LT),
	member(ET,LT),
	estacao_linha(ET,LET),
	caminho(ET,LET,ED,LED,[Linha|LVisitadas],LFinal).
	
%__________________________________________________________________
%definitivamente menor caminho
menor_caminho(Eorigem, Edestino, CF):-
	findall(Caminho,gera_caminho(Eorigem, Edestino, Caminho), [CH|CT]),
	length(CH,Length),
	menor_caminho(CH,Length,CT,CF).

%% obtem o mais pequeno
menor_caminho(CF,_,[],CF).
menor_caminho(_,Length,[CH|CT],CF):-
	length(CH,Length1),
	Length1 =< Length,
	menor_caminho(CH,Length1,CT,CF),
	!.
menor_caminho(CC,Length,[_|CT],CF):-
	menor_caminho(CC,Length,CT,CF).

%__________________________________________________________________________________________________________
%__________________________________________________________________________________________________________
%Outra resolucao:
% Exercício 2

:-dynamic cruza/3,estaçăo_linhas/2,estaçőes/1.


linha(1,[a,b,c,d,e,f]).
linha(2,[g,b,h,i,j,k]).
linha(3,[l,j,m,n,o,d,p]).
linha(4,[f,q,r,s,t]).
linha(5,[t,u,j,v,a,g]).


inicializa:- gera_cruzamentos,gera_estaçőes,gera_estaçőes_linhas.

gera_cruzamentos:-findall(_,cruzamento,_).

cruzamento:-	linha(N1,LE1),
		linha(N2,LE2),
		N1\==N2,
		intersecçăo(LE1,LE2,[H|T]),
		assertz(cruza(N1,N2,[H|T])).
%		intersecçăo(LE1,LE2,LI),
%		assertz(cruza(N1,N2,LI)).

gera_estaçőes:-	findall(L,linha(_,L),LE),
		une(LE,LEstaçőes),
		asserta(estaçőes(LEstaçőes)).

une([LH|LT],LU):-une(LT,LU1),uniăo(LH,LU1,LU).
une([],[]).


gera_estaçőes_linhas:- 
  findall(_,
	  (estaçőes(LE),member(E,LE),todas_linhas(E,LL),assertz(estaçăo_linhas(E,LL))),
	  _).

todas_linhas(E,LL):-
	findall(Linha,(linha(Linha,LEL),member(E,LEL)),LL).

gera_caminho(E1,E2,LC):-
		estaçăo_linhas(E1,LE1),estaçăo_linhas(E2,LE2),caminho(E1,LE1,E2,LE2,[],LC).


caminho(E1,LE1,E2,LE2,_,[(E1,E2,Linha)]):-intersecçăo(LE1,LE2,[H|T]),member(Linha,[H|T]),!.
caminho(E1,LE1,E2,LE2,LLV,[(E1,EI,Linha)|LC]):-member(Linha,LE1),
					(not member(Linha,LLV)),
					cruza(Linha,_,L),
					member(EI,L),
					estaçăo_linhas(EI,LEI),
					caminho(EI,LEI,E2,LE2,[Linha|LLV],LC).

mtl(E1,E2,LMTL):-findall(L,gera_caminho(E1,E2,L),LL),menor(LL,LMTL).

menor([H],H).
menor([H|T],H):-menor(T,L1),length(H,C),length(L1,C1),C<C1,!.
menor([H|T],L1):-menor(T,L1).

uniăo([],L,L).
uniăo([X|L],L1,L2):-member(X,L1),!,uniăo(L,L1,L2).
uniăo([X|L],L1,[X|L2]):-uniăo(L,L1,L2).

escreve([H|T]):-
	write(H),nl,escreve(T).
escreve([]).

intersecçăo([],_,[]).
intersecçăo([X|L],L1,[X|L2]):-member(X,L1),!,intersecçăo(L,L1,L2).
intersecçăo([_|L],L1,L2):-intersecçăo(L,L1,L2).	
	
	